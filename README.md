# Agrotoxin project

This repository contains code for processing the pesticides screen data described in:  
Gandara L, Jacoby R, Laurent F, Spatuzzi M, Vlachopoulos N, Borst N, Ekmen G, Potel C, Garrido-Rodriguez M,  Böhmert AL, Misunou N, Bartmanski BJ, Li XC, Kutra D, Hériché J-K, Tischer C, Zimmermann-Kogadeeva M, Ingham V, Savitski M, Masson J-B, Zimmermann M, Crocker J,. **Pervasive sublethal effects of agrochemicals on insects at environmentally relevant concentrations**. Science 386, 446-453 (2024). [DOI:10.1126/science.ado0251](https://doi.org/10.1126/science.ado0251) (early version on [bioRxiv](https://www.biorxiv.org/content/10.1101/2024.01.12.575373v1)).

You can browse the data at https://agrotoxin.embl.de


## Overview

The overall workflow comprises several components:
1. Extracting and classifying larval body movements with LarvaTagger. This pipeline has its own repository: https://gitlab.com/larvataggerpipelines/pesticides
2. [Reformatting LarvaTagger output](0-LarvaTagger_data_reformatting) for subsequent use (e.g. exploratory analysis and hits identification).
3. [Tracking larvae](1-tracking) with ilastik
4. [Extracting features from larvae trajectories](2-trajectory_features_extraction)
5. [Identifying compounds significantly affecting larval behavior](3-hits_identification)

The data directory contains metadata files (e.g. screen layout, information on compounds used) and additional data (e.g. lethality information).  
All scripts are meant to be run from their respective directory.  

## Requirements

* A compute cluster with the Lmod environment module system and the Slurm job scheduler
* Nextflow, Singularity/Apptainer, Java, Python, R and RStudio

## Running the code

See README file in each subdirectory.

## Data organisation

Data has been deposited in the BioImage Archive under accession number [S-BIAD970](https://www.ebi.ac.uk/biostudies/BioImages/studies/S-BIAD970).

### Primary data
The original data consists of 70 TB of video files in uncompressed DIB format in an avi container. They are under a `Videos` directory:  
`Videos/A00<XX>DF0000<YY>/<WellCoordinates>_<datetime>.avi`  

where 

- XX indicate replicates
- YY is code for the plate
- WellCoordinates: position of the well in the plate with rows identified by letters and columns by numbers, e.g. C3
- datetime: file creation time stamp as YYYY-MM-DD-hhmmss-0000

### Analysis data
Output of the preprocessing and tracking workflows is under an `analysis` directory with one subdirectory for each plate eventually containing the following files:
1. output of preprocessing steps:  
    - `*--mov.tif`: movie of same length and shape as the input, but with inverted grayscale and with signal outside the well removed
    - `*--well.txt`: text file containing the well center coordinates and the well width
    - `*--max.tif`
    - `*--max_with_well.tif`
    - `*--raw.tif`
    - `*--sum.tif`
    - `*--well.tif`
 2. output of ilastik tracking:
    - `*--pc.h5`: pixel classification result in hdf5 - needed as an input for tracking.
  In order to load this file for examination with Fiji, use the [ilastik Fiji plugin](https://www.ilastik.org/documentation/fiji_export/plugin).
    - `*--pc.png`: "flattened" pixel classification result.
    - `*--tracking-table.csv`: Features computed by ilastik. Descriptions can be found [here](https://www.ilastik.org/documentation/tracking/tracking#sec_Plugin) and [here](https://www.ilastik.org/documentation/objects/objectfeatures#standard-object-features)
    - `*--tracking-oids.h5`: label image of tracking result - where pixel values match up to `labelimageId` in the table.
    - `*--tracking-flat.png`: "flattened" tracking result.
    - `*--tracking.gif`: Movie of tracking result as gif.

The total volume of this data is 4.2 TB.

#### File versioning scheme
Each file name **must include version information** in the form --W-X-Y-- where 
- W - Preprocessing version
- X - Training version
- Y - Ilastik batch output

Note: These are added sequentially at each step, i.e. a file output by the preprocessing step has only W, a file output by the ilastik workflow has W-X-Y.


## Authors

Dominik Kutra  
Christian Tischer  
Matteo Spatuzzi  
Jean-Karim H&eacute;rich&eacute;  
