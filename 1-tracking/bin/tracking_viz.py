#!/usr/bin/env python

import logging
import os
import sys
from argparse import ArgumentParser

import h5py
import imageio
import matplotlib
matplotlib.use("agg")
import numpy as np
import pandas as pd
import vigra

from matplotlib import pyplot as plt

logger = logging.getLogger(__name__)

def parse_args():
    p = ArgumentParser(
        description="Generate preview images of tracking results given csv table and label image",
    )

    p.add_argument(
        "table",
    )
    p.add_argument(
        "labelimage",
    )
    p.add_argument(
        "outputbase",
    )
    p.add_argument(
        "-v", "--verbose",
        action="store_true")

    args = p.parse_args()
    return args

def init_logging(verbose):
    level = logging.DEBUG if verbose else logging.INFO
    logger.setLevel(level)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(level)
    formatter = logging.Formatter("%(asctime)s %(levelname)s %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)



def get_frame_oid_dict(table, frame):
    """Maps labelimageId -> lineageId"""
    tmp = {
            k: v
            for k, v in table.loc[table["frame"] == frame][
                ["labelimageId", "lineageId"]
            ].values
        }
    tmp[0] = 0
    return tmp


def read_h5(filename, dataset="exported_data"):
    with h5py.File(filename, "r") as f:
        ds = f[dataset]
        assert "axistags" in ds.attrs
        return vigra.taggedView(
            ds[()], axistags=vigra.AxisTags.fromJSON(ds.attrs["axistags"])
        )


def plot_image_to_file(data, fname, dpi, fig_kwargs, imshow_kwargs):
    f = plt.figure(**fig_kwargs)
    s = f.add_subplot(111)
    s.imshow(data, **imshow_kwargs)
    s.set_axis_off()
    f.savefig(fname, bbox_inches="tight", pad_inches=0, dpi=dpi)
    plt.close(f)


def main():
    args = parse_args()
    init_logging(args.verbose)

    logger.info(f"reading table from {args.table}")
    table = pd.read_csv(args.table)
    lineage_ids = [0] + table["lineageId"].unique().tolist()

    cmap = matplotlib.colormaps["hsv"]
    vals = np.linspace(0, 1, len(lineage_ids))
    np.random.shuffle(vals)
    cmap_listed = matplotlib.colors.ListedColormap(cmap(vals))
    cmap_listed.colors[0] = [0.0, 0.0, 0.0, 1.0]

    lineage_id_to_index = {lineageid: idx for idx, lineageid in enumerate(lineage_ids)}

    logger.info(f"reading data from {args.labelimage}")
    data = read_h5(args.labelimage, dataset="exported_data")
    assert data.axistags.keys() == ["t", "y", "x", "c"]

    tracking_flat = np.zeros(data.shape[1:3], dtype="uint32")

    dpi = 80
    figsize = tuple(np.array(tracking_flat.shape) / dpi)

    filenames = []
    logger.info(f"writing {data.shape[0]} intermediate timeframes")
    for timeframe in range(data.shape[0]):
        tf_fname = f"frame_{timeframe:04d}.png"
        logger.debug(f"writing timeframe {timeframe} -> {tf_fname}")
        object_id_to_lineage_id = get_frame_oid_dict(table, timeframe)

        the_dict = {
            k: lineage_id_to_index[v] for k, v in object_id_to_lineage_id.items()
        }
        tf = data[timeframe, ...].squeeze().copy()
        logger.debug("max", tf.max())
        unique_oids = np.unique(tf)
        if any([x not in the_dict for x in unique_oids]):
            logger.warning(f"frame {timeframe}: problem with uids not in the table {unique_oids}")

        nx = np.vectorize(lambda x: the_dict.get(x, 0))(tf)
        assert nx.shape == tracking_flat.shape
        tracking_flat[nx != 0] = nx[nx != 0]

        plot_image_to_file(
            nx,
            tf_fname,
            dpi=dpi,
            fig_kwargs={"figsize": figsize},
            imshow_kwargs={"cmap": cmap_listed, "interpolation": "nearest", "vmin": 0, "vmax": len(lineage_ids)},
        )

        filenames.append(tf_fname)

    anim_fname = f"{args.outputbase}--tracking.gif"
    with imageio.get_writer(anim_fname, mode="I") as writer:
        logger.info(f"writing gif animation to {anim_fname}")
        for filename in filenames:
            image = imageio.imread(filename)
            writer.append_data(image)

    for filename in set(filenames):
        os.remove(filename)

    flat_fname = f"{args.outputbase}--tracking-flat.png"
    logger.info(f"writing flattened overview to {anim_fname}")

    plot_image_to_file(
        tracking_flat,
        flat_fname,
        dpi=dpi,
        fig_kwargs={"figsize": figsize},
        imshow_kwargs={"cmap": cmap_listed, "interpolation": "nearest",  "vmin": 0, "vmax": len(lineage_ids)},
    )


if __name__ == "__main__":
    main()
