#!/usr/bin/env python

import sys
from pathlib import Path

import h5py
import matplotlib
matplotlib.use("agg")

import numpy as np
import vigra

from matplotlib import pyplot as plt

if __name__ == "__main__":
    img = Path(sys.argv[1])
    assert img.exists()

    outp = Path(sys.argv[2])

    with h5py.File(img, "r") as f:
        data = f["exported_data"][()][..., (0,)]
        axistags = vigra.AxisTags.fromJSON(f["exported_data"].attrs["axistags"])
    vdata = vigra.taggedView(data, axistags=axistags)
    vdata = np.arange(vdata.shape[0]).reshape((vdata.shape[0], 1, 1, 1)) * vdata
    maxproj = vdata.max(axis="t")

    dpi = 80
    figsize = tuple(np.array(maxproj.squeeze().shape) / dpi)

    f = plt.figure(figsize=figsize)
    s = f.add_subplot(111)
    s.imshow(maxproj, cmap="viridis")
    s.set_axis_off()
    f.savefig(outp, bbox_inches="tight", pad_inches=0, dpi=dpi)
