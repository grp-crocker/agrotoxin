

// override this to use a different container/ilastik version
params.ilastikVersion = "1.4.0"

// these names should stay constant
params.pcProject = "Dros_larvae_pixel_class.ilp"
params.trackingProject = "Dros_larvae_tracking_full.ilp"

// override this to use a different training
params.ilastikProjectVersion = "2"

nextflow.enable.dsl=2


def getOutputNameBase(basename) {
  (uuid, versions) = basename.split("--")
  newVersion = [versions, params.ilastikProjectVersion, params.ilastikVersion].join("-")

  return [uuid, newVersion].join("--")
}

process pixelClassification {
  cpus 8
  memory { 72.GB * task.attempt }
  errorStrategy { ( task.exitStatus in 134..143 ) ? 'retry' : 'terminate' }
  maxRetries 2
  time { 30.minutes * task.attempt }

  // Unfortunately this can break - the _cv2 bit comes from biocontainer
  // this indicates the second build. So in principle, whenever there is a
  // version bump, one would have to reset _cv2 to _cv1
  container 'bin/ilastik-' + params.ilastikVersion + '_cv2.sif'

  input:
  tuple val(basename), val(plateName), path(imagefile)

  publishDir {
    params.outputDir + "/" + plateName
  }, mode: "copy", overwrite: true

  output:
  tuple val(basename), path('*--pc.h5'), path('*--pc.png')

  // ilastik doesn't understand the workdir very well - per default workdir is the executable location
  // hence we have to give absolute paths to everything
  // also: .baseName will be unescaped, while blank path has escapes...
  shell:
  outputBaseName = getOutputNameBase(basename)
  pcProject = params.projectsPath + "/" + params.ilastikProjectVersion + "/" + params.pcProject

  '''
  LAZYFLOW_THREADS=8
  LAZYFLOW_TOTAL_RAM_MB=73728
  run_ilastik.sh --headless \
    --project !{pcProject} \
    --output_format "compressed hdf5" \
    --output_filename_format "${PWD}/!{outputBaseName}--pc.h5" \
    --raw_data ${PWD}/!{imagefile} \
    --export_source "Probabilities" \
    --logfile "${PWD}/ilastiklog.txt"

  # location of python inside the container :grimacing:
  PATH=/opt/ilastik-!{params.ilastikVersion}-Linux/bin:${PATH}
  gen_projection.py "${PWD}/!{outputBaseName}--pc.h5" "${PWD}/!{outputBaseName}--pc.png"
  '''
}

process tracking {
  cpus 4
  memory '18 GB'
  time '3 h'
  container 'bin/ilastik-' + params.ilastikVersion + '_cv2.sif'

  input:
  tuple val(basename), val(plateName), path(raw_data), path(probabilities)

  publishDir {
    params.outputDir + "/" + plateName
  }, mode: "copy", overwrite: true

  output:
  tuple val(basename), path('*--tracking-table.csv'), path('*--tracking-oids.h5'), path('*--tracking-flat.png'), path('*--tracking.gif')

  shell:
  outputBaseName = getOutputNameBase(basename)
  trackingProject = params.projectsPath + "/" + params.ilastikProjectVersion + "/" + params.trackingProject

  '''
  LAZYFLOW_THREADS=4
  LAZYFLOW_TOTAL_RAM_MB=12288

  # Do tracking twice, once for
  # the table
  run_ilastik.sh --headless \
    --logfile "${PWD}/ilastiklog_table.txt" \
    --project !{trackingProject} \
    --raw_data ${PWD}/!{raw_data} \
    --prediction_maps ${PWD}/!{probabilities} \
    --export_source="Plugin" \
    --export_plugin="CSV-Table"

  # rename has to be done as ilastik currently doesn't allow specifying the
  # table filename :grimacing:
  mv *CSV-Table.h5.csv !{outputBaseName}--tracking-table.csv

  # once for the label image
  run_ilastik.sh --headless \
    --project !{trackingProject} \
    --logfile "${PWD}/ilastiklog_oids.txt" \
    --output_format "compressed hdf5" \
    --output_filename_format "${PWD}/!{outputBaseName}--tracking-oids.h5" \
    --raw_data ${PWD}/!{raw_data} \
    --prediction_maps ${PWD}/!{probabilities} \
    --export_source "Object-Identities"

  # location of python inside the container :grimacing:
  PATH=/opt/ilastik-!{params.ilastikVersion}-Linux/bin:${PATH}
  tracking_viz.py \
    "${PWD}/!{outputBaseName}--tracking-table.csv" \
    "${PWD}/!{outputBaseName}--tracking-oids.h5" \
    !{outputBaseName}

  '''
}


// workflow
//
inp_files = Channel.fromPath(params.inputFiles, checkIfExists: true)
              .map({ tuple it.baseName, it.getParent().getName(), it })

workflow {

  x = pixelClassification(inp_files)
  tmp = x.map({ tuple it[0], it[1]})
  ch = inp_files.join(tmp)
  ch2 = tracking(ch)
}


