package de.embl.cba.larvae.util;

import ij.ImagePlus;
import ij.io.FileSaver;

import java.io.File;

public class TIFFSaver
{
	public static void saveAsTIFF( ImagePlus imagePlus, File file )
	{
		System.out.println( "Save " + file );
		imagePlus.setTitle( file.getName() );
		final FileSaver saver = new FileSaver( imagePlus );
		saver.saveAsTiff( file.getPath() );
	}
}
