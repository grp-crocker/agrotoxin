package de.embl.cba.larvae.cmd;

import de.embl.cba.larvae.Version;
import de.embl.cba.larvae.util.StackSubSetter;
import de.embl.cba.larvae.util.TIFFSaver;
import ij.ImagePlus;
import ij.ImageStack;
import ij.plugin.AVI_Reader;
import net.imagej.ImageJ;
import org.apache.commons.io.FilenameUtils;

import java.io.File;

public class AVIConverterCmd
{
	public static final String OUT = "raw";
	public static final String SUFFIX = Version.SEP_VER_SEP + OUT;

	public static void main( String... args )
	{
		String aviPath = args[ 0 ];
		String outputDirectory = args[ 1 ];
		int useEveryNthFrame = Integer.parseInt( args[ 2 ] );
		boolean headless = args.length > 3 ? Boolean.parseBoolean( args[ 3 ] ) : true;

		if ( ! headless ) new ImageJ().ui().showUI();

		process( aviPath, outputDirectory, headless, useEveryNthFrame );

		if ( headless ) System.exit( 0 );
	}

	/**
	 * @param moviePath
	 * @param outputDirectory
	 * @param headless
	 * @param useEveryNthFrame
	 */
	private static synchronized void process( String moviePath, String outputDirectory, boolean headless, int useEveryNthFrame )
	{
		// open
		//
		System.out.println("Opening " + moviePath + "...");
		final AVI_Reader reader = new AVI_Reader();
		final ImageStack imageStack = reader.makeStack( moviePath, 1, 0, false, true, false );
		ImagePlus subset = new ImagePlus( ( new File( moviePath ) ).getName(), imageStack );

		// subsample in time
		//
		if ( useEveryNthFrame > 1 )
		{
			System.out.println( "Subset every " + useEveryNthFrame + " frame.");
			final StackSubSetter subSetter = new StackSubSetter();
			subset = subSetter.subset( subset, useEveryNthFrame );
		}

		// save
		//
		final String fileName = new File( moviePath ).getName();
		//final String parentFolderName = new File( parentFolder ).getName();
		final String baseFileName = FilenameUtils.removeExtension( fileName ) ;
		subset.setDimensions( 1, 1, subset.getStackSize() );
		final String outFileName = baseFileName + SUFFIX + ".tif";
		TIFFSaver.saveAsTIFF( subset, new File( outputDirectory, outFileName ) );

		if ( ! headless ) subset.show();

		System.out.println( "Done!" );
	}
}
