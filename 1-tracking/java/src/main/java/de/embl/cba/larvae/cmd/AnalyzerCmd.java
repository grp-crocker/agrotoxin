package de.embl.cba.larvae.cmd;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.OvalRoi;
import ij.gui.Roi;
import ij.io.FileSaver;
import ij.plugin.AVI_Reader;
import ij.plugin.RoiEnlarger;
import ij.plugin.ZProjector;
import ij.process.AutoThresholder;
import ij.process.ImageProcessor;
import sc.fiji.analyzeSkeleton.SkeletonResult;
import sc.fiji.skeletonize3D.Skeletonize3D_;
import sc.fiji.analyzeSkeleton.AnalyzeSkeleton_;
import net.imagej.ImageJ;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class AnalyzerCmd
{
	private static Map< String, Object > properties = new HashMap<>();

	// TODO
	public static void main( String... args ) throws IOException
	{
		String maxProjectionPath = args[ 0 ];
		boolean headless = args.length > 1 ? Boolean.parseBoolean( args[ 1 ] ) : true;
		boolean exit = args.length > 2 ? Boolean.parseBoolean( args[ 2 ] ) : headless;

		if ( ! headless ) new ImageJ().ui().showUI();

		run( maxProjectionPath, headless );

		if ( exit ) System.exit( 0 );
	}

	private static synchronized void run( String maxProjectionPath, boolean headless) throws IOException
	{
		// open
		//
		ImagePlus maxProjection = IJ.openImage( maxProjectionPath );

		// binarisation
		// probably make a function that does that
		final Roi enlarge = RoiEnlarger.enlarge( maxProjection.getRoi(), -maxProjection.getWidth() / 10 );
		maxProjection.setRoi( enlarge );
		maxProjection.getProcessor().setAutoThreshold( AutoThresholder.Method.Minimum, true );
		final ImagePlus mask = new ImagePlus( "mask", maxProjection.getProcessor().createMask() );
		maxProjection.getProcessor().resetThreshold();

		if ( ! headless ) maxProjection.show();
		if ( ! headless ) mask.show();


		// both for one image to estimate the number of larvae
		// and for the maximum projection


		// Maybe remove background shading:
		// - median to make the shading more homogeneous
		// - white top hat to remove the shading
		//
		// global auto-threshold, "minimum" works well
		// minimum will work even better in the well center where
		// there is a good amount of worms with respect to BG
		// the ROI is respected for the computation of the threshold
		// thus keeping the well ROI is useful, maybe even shrinking it a bit
		// to not consider the shading so much which is at the well edges
		// RoiEnlarger.enlarge(imp, -10);

		// loop through movie (every nth frame)
		// - binarize
		// - connected components
		// - find the image with the maximum number of connected components
		//   above a certain size

		// get the areas and intensities

		// find the intensity of one worm

		// binarize maximum projection

		// skeletonize
		final ImagePlus thin = mask.duplicate();
		thin.setTitle( "thin" );
		final Skeletonize3D_ skeletonize = new Skeletonize3D_();
		skeletonize.setup( "", thin );
		skeletonize.run( thin.getProcessor() );
		if ( ! headless ) thin.show();

		// analyze skeleton
		final AnalyzeSkeleton_ analyzeSkeleton = new AnalyzeSkeleton_();
		analyzeSkeleton.setup( "", thin );
		final SkeletonResult skeletonResult = analyzeSkeleton.run();
		final int[] numBranches = skeletonResult.getBranches();
		final double[] averageBranchLength = skeletonResult.getAverageBranchLength();
		double totalLength = 0;
		int totalNumBranches = 0;
		final int numTrees = numBranches.length;
		System.out.println("Number of trees : " + numTrees );
		for ( int treeIndex = 0; treeIndex < numTrees; treeIndex++ )
		{
			totalNumBranches += numBranches[ treeIndex ];
			totalLength += numBranches[ treeIndex ] * averageBranchLength[ treeIndex ];
		}
		System.out.println("Number of branches : " + totalNumBranches );
		System.out.println("Length : " + totalLength );


		// measure skeleton length

		// measure skeleton number of branches

		// measure the mean and sdev of the intensity within the
		// skeletonized region => worm thickness and distribution?
		// - just binarization of the whole skeleton and then analysis in MLJ
		//   would be the easiest, but one could also do an intensity analysis per branch

		// done
		//
		System.out.println( "Preprocess " + new File( maxProjectionPath ).getName() + " done!" );
	}

	private static OvalRoi getWellRoi( ImageProcessor closing, int wellMargin )
	{
		final int height = closing.getHeight();
		final int width = closing.getWidth();
		int ys = (int) (height * 0.4);
		int ye = (int) (height * 0.6);
		//final int[][] borders = new int[ ye - ys + 1 ][ 2 ];
		double wellWidth = 0, wellCenterX = 0, wellCenterY = 0, numWidths = 0;
		for ( int y = ys; y <= ye; y++ )
		{
			int xLeft = 0, localWidth = 0;
			for ( int x = 0; x < width - 1; x++ )
			{
				final int v = closing.get( x, y );
				final int vNext = closing.get( x + 1, y );
				if ( xLeft == 0 )
				{
					if ( vNext < v )
					{
						// inner left border
						xLeft = x;
					}
				}
				else if ( vNext > v )
				{
					// inner right border
					localWidth = x - xLeft;
					break;
				}
			}

			if ( localWidth > wellWidth )
			{
				wellWidth = localWidth;
				wellCenterX = localWidth / 2.0 + xLeft;
				wellCenterY = y;
				numWidths = 1;
			}

			if ( localWidth == wellWidth )
			{
				// adjust y position
				wellCenterY = ( numWidths * wellCenterY + y ) /  ( numWidths + 1 );
				numWidths++;
			}
		}
		wellWidth -= wellMargin; // safety zone
		System.out.println("Well center = " + wellCenterX + "," + wellCenterY );
		System.out.println("Well width = " + wellWidth );
		System.out.println("Number of positions with equal well width = " + numWidths );

		properties.put( "wellWidth", wellWidth );
		properties.put( "wellCenterX", wellCenterX );
		properties.put( "wellCenterY", wellCenterY );

		final OvalRoi wellRoi = new OvalRoi( wellCenterX - wellWidth/2, wellCenterY - wellWidth/2, wellWidth, wellWidth );
		return wellRoi;
	}

	private static void saveAsTIFF( ImagePlus movie, File movieFile )
	{
		System.out.println( "Save " + movieFile );
		movie.setTitle( movieFile.getName() );
		final FileSaver saver = new FileSaver( movie );
		saver.saveAsTiff( movieFile.getPath() );
	}

	private static ImagePlus openAVI( String aviPath )
	{
		System.out.println("Open " + aviPath );
		return AVI_Reader.open( aviPath, false );
	}

	private static void saveProperties( String outputFilePath, Map< String, Object > map ) throws IOException
	{
		// new file object
		File file = new File(outputFilePath);

		BufferedWriter bf = null;

		try {

			// create new BufferedWriter for the output file
			bf = new BufferedWriter(new FileWriter(file));

			// iterate map entries
			for ( Map.Entry<String, Object> entry :
					map.entrySet()) {

				// put key and value separated by a colon
				bf.write(entry.getKey() + ":"
						+ entry.getValue() );

				// new line
				bf.newLine();
			}

			bf.flush();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally {

			try {

				// always close the writer
				bf.close();
			}
			catch (Exception e) {
			}
		}
	}
}
