package de.embl.cba.larvae;

public class Version
{
	public static final String version = "0.5.2";
	public static final String sep = "--";
	public static final String SEP_VER_SEP = Version.sep + Version.version + Version.sep;
}
