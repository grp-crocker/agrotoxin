package de.embl.cba.larvae.cmd;

import de.embl.cba.larvae.Version;
import de.embl.cba.larvae.util.CircleFitter;
import fiji.threshold.Auto_Local_Threshold;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.OvalRoi;
import ij.io.Opener;
import ij.plugin.ZProjector;
import ij.plugin.filter.RankFilters;
import ij.process.ByteProcessor;
import ij.process.ImageProcessor;
import inra.ijpb.binary.BinaryImages;
import net.imagej.ImageJ;

import java.awt.geom.Point2D;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static de.embl.cba.larvae.util.TIFFSaver.saveAsTIFF;

public class PreProcessorCmd
{
	private static ZProjector projector;
	private static Map< String, Object > properties = new HashMap<>();

	public static void main( String... args ) throws IOException, CircleFitter.LocalException
	{
		String filePath = args[ 0 ];
		String outputDir = args[ 1 ];

		boolean headless = args.length > 2 ? Boolean.parseBoolean( args[ 2 ] ) : true;
		boolean exit = args.length > 3 ? Boolean.parseBoolean( args[ 3 ] ) : headless;

		if ( ! headless ) new ImageJ().ui().showUI();

		final int wellMargin = 20;
		process( filePath, outputDir, headless, wellMargin );

		if ( exit ) System.exit( 0 );
	}

	private static synchronized void process( String moviePath, String outputDirectory, boolean headless, int wellMargin ) throws IOException, CircleFitter.LocalException
	{
		// open
		//
		System.out.println("Open " + moviePath);
		ImagePlus movie = (new Opener()).openImage( moviePath );
		movie.deleteRoi();

		// invert
		//
		System.out.println("Invert");
		ImageStack stack = movie.getStack();
		for (int slice = 1; slice <= stack.getSize(); slice++) {
			ImageProcessor ip = stack.getProcessor(slice);
			ip.invert();
		}

		// remove background gradient
		// added 17. October 2023 to accommodate changes in the input data
		//
		System.out.println("Remove background");
		RankFilters rankFilters = new RankFilters();
		for ( int t = 1; t <= movie.getStackSize(); t++ )
		{
			// For a discussion on how the radius of the filter was chosen, see: https://git.embl.de/grp-cba/larvae-tracking-classification/-/issues/51
			rankFilters.rank( movie.getStack().getProcessor( t ), 30, RankFilters.TOP_HAT );
		}

		// create max projection for QC
		//
		System.out.println("Create maximum projection");
		projector = new ZProjector();
		projector.setImage( movie );
		projector.setMethod( ZProjector.MAX_METHOD );
		projector.doProjection();
		final ImagePlus maxProjection = projector.getProjection();

		//
		// determine well boundaries
		//

		// minimum projection
		//
		// TODO: maybe we don't need this, but could determine the
		//   well boundaries just from the first image?!
		//   main advantage would be to save computation time
		System.out.println("Minimum projection");
		projector = new ZProjector();
		projector.setImage( movie );
		projector.setMethod( ZProjector.MIN_METHOD );
		projector.doProjection();
		final ImagePlus minProjection = projector.getProjection();

		// threshold
		//
		final ImagePlus binary = ( ImagePlus ) new Auto_Local_Threshold().exec( minProjection, "Bernsen", 15, 40, 0, true )[0];
		if ( ! headless ) binary.show();
		// remove small objects
		final ImageProcessor areaOpening = BinaryImages.areaOpening( binary.getProcessor(), 500 );
		final ImagePlus binaryOpen = new ImagePlus( "remove small", areaOpening );
		if ( ! headless ) binaryOpen.show();

		// find inner well edges
		//
		ImageProcessor input = binaryOpen.getProcessor();
		final int ny = input.getHeight();
		final int nx = input.getWidth();
		final int nxHalf = nx / 2;
		final ByteProcessor innerEdges = new ByteProcessor( nx, ny );
		ArrayList< Point2D.Double > edgePoints = new ArrayList();

		int dy = ny / 20; // other-wise the circle fitting is very slow
		for ( int y = 0; y < ny; y++ )
		{
			// left
			for ( int x = nxHalf; x > 1; x-- )
			{
				if ( input.get( x, y ) < input.get( x - 1, y ) )
				{
					if ( y % dy == 0 )
					{
						edgePoints.add( new Point2D.Double( x, y ) );
						innerEdges.putPixel( x, y, 255 );
					}
					else
					{
						innerEdges.putPixel( x, y, 100 );
					}
					break;
				}
			}

			// right
			for ( int x = nxHalf; x < nx - 1; x++ )
			{
				if ( input.get( x, y ) < input.get( x + 1, y )  )
				{
					if ( y % dy == 0 )
					{
						innerEdges.putPixel( x, y, 255 );
						edgePoints.add( new Point2D.Double( x, y ) );
					}
					else
					{
						innerEdges.putPixel( x, y, 100 );
					}
					break;
				}
			}
		}

		final ImagePlus innerEdgesImp = new ImagePlus( "inner edges", innerEdges );
		if ( ! headless ) innerEdgesImp.show();

		// fit a circle
		CircleFitter fitter = new CircleFitter();
		fitter.initialize(edgePoints.toArray(new Point2D.Double[edgePoints.size()]), 3 * dy);
		int wellWidth = ( int ) (fitter.getQuantileTriangleRadius() * 2);
		wellWidth -= 2 * wellMargin;
		int wellCenterX = ( int ) fitter.getMedianTriangleCenter().x;
		int wellCenterY = ( int ) fitter.getMedianTriangleCenter().y;
		properties.put( "wellWidth", wellWidth );
		properties.put( "wellCenterX", wellCenterX );
		properties.put( "wellCenterY", wellCenterY );

		final OvalRoi wellRoi = new OvalRoi( wellCenterX - wellWidth/2, wellCenterY - wellWidth/2, wellWidth, wellWidth );

		// remove signal outside the well border
		//

		// movie
		clearOutsideROI( movie, wellRoi );

		// maximum projection
		final ImagePlus maxWithoutWell = maxProjection.duplicate();
		clearOutsideROI( maxWithoutWell, wellRoi );

		// create sum projection for eventual phenotypic analysis
		//
		projector = new ZProjector();
		projector.setImage( movie );
		projector.setMethod( ZProjector.SUM_METHOD );
		projector.doProjection();
		final ImagePlus sumProjection = projector.getProjection();

		// save
		//
		String baseFileName = new File( moviePath ).getName();
		baseFileName = baseFileName.replaceAll( "--[\\d]..*--.*", "" );
		baseFileName = baseFileName + Version.SEP_VER_SEP;
		final int numFrames = movie.getStack().size();

		// processed movie
		movie.setDimensions( 1, 1, numFrames );
		movie.setRoi( wellRoi );
		saveAsTIFF( movie, new File( outputDirectory, baseFileName + "mov.tif" ) );

		// sumProjection
		sumProjection.setRoi( wellRoi );
		saveAsTIFF( sumProjection, new File( outputDirectory, baseFileName + "sum.tif" ) );

		// maxProjection with well signal
		maxProjection.setRoi( wellRoi );
		saveAsTIFF( maxProjection, new File( outputDirectory, baseFileName + "max_with_well.tif" ) );

		// maxProjection
		maxWithoutWell.setRoi( wellRoi );
		saveAsTIFF( maxWithoutWell, new File( outputDirectory, baseFileName + "max.tif" ) );

		// well boundaries
		innerEdgesImp.setRoi( wellRoi );
		saveAsTIFF( innerEdgesImp, new File( outputDirectory, baseFileName + "well.tif" ) );

		// properties
		saveProperties( new File( outputDirectory, baseFileName + "well.txt" ).getPath(), properties );


		// done
		//

		if ( ! headless ) sumProjection.show();
		if ( ! headless ) movie.show();
		if ( ! headless ) maxProjection.show();

		System.out.println( "Done!" );
	}

	private static void clearOutsideROI( ImagePlus imagePlus, OvalRoi roi ) {
		// Get the stack of images
		ImageStack stack = imagePlus.getStack();

		// Set the ROI on the ImagePlus object
		imagePlus.setRoi(roi);

		// Iterate over each slice in the stack
		for (int slice = 1; slice <= stack.getSize(); slice++) {
			ImageProcessor ip = stack.getProcessor(slice);

			// Process each pixel in the slice
			for (int y = 0; y < ip.getHeight(); y++) {
				for (int x = 0; x < ip.getWidth(); x++) {
					// Check if the pixel is outside the ROI
					if (!roi.contains(x, y)) {
						// Set pixel outside the ROI to 0
						ip.putPixel(x, y, 0);
					}
				}
			}
		}

		// Remove the ROI
		imagePlus.deleteRoi();
	}


	private static void saveProperties( String outputFilePath, Map< String, Object > map ) throws IOException
	{
		// new file object
		File file = new File(outputFilePath);

		BufferedWriter bf = null;

		try {

			// create new BufferedWriter for the output file
			bf = new BufferedWriter(new FileWriter(file));

			// iterate map entries
			for ( Map.Entry<String, Object> entry :
					map.entrySet()) {

				// put key and value separated by a colon
				bf.write(entry.getKey() + ":"
						+ entry.getValue() );

				// new line
				bf.newLine();
			}

			bf.flush();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally {

			try {

				// always close the writer
				bf.close();
			}
			catch (Exception e) {
			}
		}
	}
}
