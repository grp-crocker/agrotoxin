package de.embl.cba.larvae.util;

import ij.ImagePlus;
import ij.ImageStack;
import ij.process.ImageProcessor;

/** Copied from ImageJ and slightly modified */
public class StackSubSetter
{
	public ImagePlus subset( ImagePlus inputImp, int inc ) {

		final ImageStack inputStack = inputImp.getStack();
		final int inputSize = inputStack.size();

		ImageProcessor ip;
		ImageStack outputStack = new ImageStack(inputStack.getWidth(), inputStack.getHeight()) ;

		for ( int i = 1; i <= inputSize; i += inc)
		{
			ip = inputStack.getProcessor(i);
			outputStack.addSlice("slice:" + i, ip);
		}

		ImagePlus outputImp = new ImagePlus( inputImp.getTitle(), outputStack);
		outputImp.setCalibration( inputImp.getCalibration() );
		return outputImp;
	}
}
