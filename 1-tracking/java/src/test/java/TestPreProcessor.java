import de.embl.cba.larvae.util.CircleFitter;
import de.embl.cba.larvae.cmd.PreProcessorCmd;

import java.io.IOException;

public class TestPreProcessor
{
	public static void main( String[] args ) throws IOException, CircleFitter.LocalException
	{
		final String[] arguments = new String[ 5 ];
		arguments[ 0 ] = "/Users/tischer/Desktop/lautaro/A1_2022-07-12-150920-0000--0.4.0--raw.tif"; // old
		arguments[ 0 ] = "/Users/tischer/Desktop/lautaro/A1_2023-09-07-134048-0000--0.4.0--raw.tif"; // new, with bg
		arguments[ 0 ] = "/Users/tischer/Desktop/lautaro/A4_2022-08-18-162141-0000--0.4.0--raw.tif";
		arguments[ 0 ] = "/Users/tischer/Desktop/lautaro/C4_2023-09-01-164021-0000--0.4.0--raw.tif";
		arguments[ 1 ] = "/Users/tischer/Desktop/Lautaro/analysis/";

		PreProcessorCmd.main( arguments );
	}
}
