import de.embl.cba.larvae.cmd.PreProcessorCmd;
import de.embl.cba.larvae.util.CircleFitter;

import java.io.File;
import java.io.IOException;

public class BatchTestPreProcessor
{
	public static void main( String[] args ) throws IOException, CircleFitter.LocalException
	{
		final File dir = new File( "/Users/tischer/Desktop/Lautaro/subset" );
		final File[] files = dir.listFiles( ( dir1, name ) -> name.endsWith( "_subset.tif" ) );
		for ( File file : files )
		{
			final String[] arguments = new String[ 3 ];
			arguments[ 0 ] = file.toString();
			arguments[ 1 ] = "true"; // run headless for batch analysis
			arguments[ 2 ] = "false"; // don't exit java otherwise this for-loop terminates
			PreProcessorCmd.main( arguments );
		}
	}
}
