import de.embl.cba.larvae.cmd.AVIConverterCmd;
import de.embl.cba.larvae.cmd.PreProcessorCmd;
import de.embl.cba.larvae.util.CircleFitter;

import java.io.IOException;

public class TestAVIConverter
{
	public static void main( String[] args )
	{
		args = new String[ 5 ];
		args[ 0 ] = "/Users/tischer/Desktop/lautaro/primary/A0001DF000000031/A1_2023-09-07-134048-0000.avi";
		args[ 1 ] = "/Users/tischer/Desktop/lautaro/analysis";
		args[ 2 ] = "15";
		args[ 3 ] = "true";
		AVIConverterCmd.main( args );
	}
}
