import de.embl.cba.larvae.cmd.AnalyzerCmd;
import de.embl.cba.larvae.util.CircleFitter;

import java.io.IOException;

public class TestAnalyzer
{
	public static void main( String[] args ) throws IOException, CircleFitter.LocalException
	{
		final String[] arguments = new String[ 5 ];
		arguments[ 0 ] = "/Users/tischer/Desktop/Lautaro/subset/A0001DF000000047_A1_2022-07-15-163200-0000_max.tif";
		arguments[ 1 ] = "false"; // headless
		arguments[ 2 ] = "false"; // exit
		AnalyzerCmd.main( arguments );
	}
}
