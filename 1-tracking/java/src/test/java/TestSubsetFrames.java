import de.embl.cba.larvae.cmd.PreProcessorCmd;
import de.embl.cba.larvae.util.CircleFitter;

import java.io.IOException;

public class TestSubsetFrames
{
	public static void main( String[] args ) throws IOException, CircleFitter.LocalException
	{
		final String[] arguments = new String[ 5 ];
		arguments[ 0 ] = "/Users/tischer/Desktop/Lautaro";
		//arguments[ 1 ] = "/Users/tischer/Desktop/Lautaro/issues/A0018DF000000051_A1_2022-11-11-202147-0000_subset.tif";
		arguments[ 1 ] = "/Users/tischer/Desktop/Lautaro/issues/A0017DF000000077_B4_2022-09-06-112046-0000_subset.tif";
		arguments[ 2 ] = "/Users/tischer/Desktop/Lautaro/Out/";
		arguments[ 3 ] = "1"; // dt
		arguments[ 4 ] = "false"; // headless

		PreProcessorCmd.main( arguments );
	}
}
