#!/bin/bash
# This script is shamelessly adapted from https://github.com/saalfeldlab/n5-utils, thanks @axtimwalde & co!

# This requires the module command to exist
module load Maven
module load Java/1.8.0_221

VERSION="0.5.2"
MEM=8 # FIXME

mvn clean install -Denforcer.skip
mvn -Dmdep.outputFile=cp.txt -Dmdep.includeScope=runtime dependency:build-classpath

echo '#!/bin/bash' > convert
echo '' >> convert
echo "JAR=\$HOME/.m2/repository/de/embl/cba/larvae-screen-analysis/${VERSION}/larvae-screen-analysis-${VERSION}.jar" >> convert
echo 'java \' >> convert
echo "  -Xmx${MEM}g \\" >> convert
echo '  -XX:+UseConcMarkSweepGC \' >> convert
echo -n '  -cp $JAR:' >> convert
echo -n $(cat cp.txt) >> convert
echo ' \' >> convert
echo '  de.embl.cba.larvae.cmd.AVIConverterCmd "$@"' >> convert
chmod a+x convert
echo "Installed: convert"

echo '#!/bin/bash' > preprocess
echo '' >> preprocess
echo "JAR=\$HOME/.m2/repository/de/embl/cba/larvae-screen-analysis/${VERSION}/larvae-screen-analysis-${VERSION}.jar" >> preprocess
echo 'java \' >> preprocess
echo "  -Xmx${MEM}g \\" >> preprocess
echo '  -XX:+UseConcMarkSweepGC \' >> preprocess
echo -n '  -cp $JAR:' >> preprocess
echo -n $(cat cp.txt) >> preprocess
echo ' \' >> preprocess
echo '  de.embl.cba.larvae.cmd.PreProcessorCmd "$@"' >> preprocess
chmod a+x preprocess
echo "Installed: preprocess"

rm cp.txt
