#!/usr/bin/env nextflow
nextflow.enable.dsl=2

/*  
   This workflow recursively processes a directory of movies in avi container
   format and converts them to multi-page tiff files.
   The --dt argument controls the number of movie frames to include in the tiff
   file and takes as parameter an integer n which indicates that every nth frame
   will be selected.
   
   Usage:
      nextflow run preprocess.nf --inputdir </path/to/input_dir> \
                                 --outputdir </path/to/output_dir> \
                                 --dt <n>
 */

params.dt = "15" // use every nth frame

params.convertExec = "${workflow.projectDir}/java/convert"
params.preprocessExec = "${workflow.projectDir}/java/preprocess"
    
/* Get all .avi files recursively in all inputdir subdirectories.
   We skip small avi files as they are most likely artefacts (e.g. 
   false start of recording).
   Note that baseName strips the file name extension after the last dot.*/

inputFile = Channel
    .fromPath( "${params.inputdir}/**.avi" )
    .filter { it.size() > 6000000000 }
    .map { file -> def plate = file.getParent().getName()
	  return tuple(file.baseName, plate, file) }


process avi_to_tiff {
    publishDir "${params.outputdir}/${plate}", mode: 'copy'
    
    shell '/bin/bash'
    
    memory { 16.GB * task.attempt }
    time { 30.minutes * task.attempt }
    errorStrategy { (task.exitStatus in 137..140 || task.exitStatus == 143) ? 'retry' : 'terminate' }
    maxRetries 3
    debug true
    
    input:
    tuple val(fileName), val(plate), path(aviPath)

    output:
    tuple val(plate), path("*--raw.tif"), emit: convertedFile
    
    script:
    """
    export DISPLAY=:0.0 # for IJ Java code
    [ -d "${params.outputdir}/${plate}" ] || mkdir -p "${params.outputdir}/${plate}"
    echo "Starting conversion of ${aviPath}..."
    ${params.convertExec} ${aviPath} "./" ${params.dt}
    echo "Conversion is done!"
    """
}

process preprocess {
    memory { 16.GB * task.attempt }
    time { 30.minutes * task.attempt }
    errorStrategy { ( task.exitStatus in 137..140 || task.exitStatus == 143 ) ? 'retry' : 'terminate' }
    maxRetries 3
    debug true

    input:
    tuple val(plate), path(rawFile)

    output:
    path("done.txt")

    script:
    """
    export DISPLAY=:0.0 # for IJ Java code
    echo "Preprocessing file: ${rawFile}"
    ${params.preprocessExec} "${rawFile}" "${params.outputdir}/${plate}"
    echo "${rawFile}" > done.txt
    """
}


workflow {
    avi_to_tiff(inputFile)
    preprocess(avi_to_tiff.out.convertedFile)
}
