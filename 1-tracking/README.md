# Tracking larvae with ilastik

## Requirements
These two Nexflow workflows were run on a computer cluster using the Lmod environment module system and the Slurm job scheduler. When running in a different environment, adapt the content of the nextflow.config file as needed.

The preprocessing workflow requires Java 8 and X11.  
The ilastik workflow uses singularity and needs bind paths to the data defined in the nextflow.config file. Some jobs initially failed due to high memory requirement, therefore the memory reservation for the pixel classification task is set at 72 GB.

## Preprocessing
The preprocessing workflow converts the video files to tiff format, reduces the data from 30 to 2 frames per second, inverts the value range and removes the well boundary.  
Run with  

```
module load Nextflow
nextflow run preprocess.nf --inputdir </path/to/Videos> --outputdir </path/to/analysis>
```
To recompile the java programs, make sure Maven and Java 8 modules are available (you may need to edit the script to match available versions) then:

```
cd java
./install.sh
```

## Tracking
Tracking is performed using ilastik packaged in a singularity container.
This workflow first segments the larvae using a pre-trained random forest-based classifier then uses the resulting probability maps as input for the tracking function of ilastik.  
Run with __

```
module load Nextflow
nextflow run ilastik.nf -w </path/to/workdir> --inputFiles </path/to/analysis/**--mov.tif --outputDir </path/to/analysis --projectsPath </path/to/agrotoxin/1-tracking/ilastik_classifiers> --ilastikProjectVersion 2
```
