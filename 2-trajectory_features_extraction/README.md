# Computing features from larvae trajectories

This R script goes through all plates in the analysis directory, reads the tracking table files for each well, compute features from each larva's trajectory, summarizes the data by well and adds columns required for use with the Image Data Explorer then concatenates all tables into one file.  
Run with:
```
Rscript compute_trajectory_features.R --input_dir </path/to/data/root/dir>
```
The data root directory is the directory that contains the `Videos` and `analysis` subdirectories.
The output is a tab-delimited file `../data/data--W-X-Y--well_summaries.csv` where W, X and Y are the version numbers of the preceding steps.
