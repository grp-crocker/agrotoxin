# Reformating LarvaTagger output

LarvaTagger output consists of one json file per well which we want to collect into a single table augmented with additional information for exploratory analysis.

For historical reasons, the process is in two steps:  
1. Convert the json files to csv.  
From this repository root directory, run:

```
python 0-LarvaTagger_data_reformating/LT_celltracking_to_larva_data.py </path/to/LarvaTagger/output>
```
This creates a file Behaviour_LT_predicted.*_larva_data.csv in each well directory of the LarvaTagger output.

2. Combine the files and some extra info from files in the data directory.  
From this repository root directory, run:

```
Rscript 0-LarvaTagger_data_reformating/LT_larva_data_builder.R --input_dir </path/to/LarvaTagger/output>
```

The output is file `data/LT_annotated_larva_data.csv`
