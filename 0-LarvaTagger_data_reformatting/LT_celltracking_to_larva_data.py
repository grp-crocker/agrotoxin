# Label reader

import numpy as np
import pandas as pd
import os
import json
import sys

behaviour_legend = {'cast_large': 1, 'small_motion': 2, 'run_large': 3, 'hunch_large': 4, 'roll_large': 5, 'stop_large': 6, 'back_large': 7}

label_list = list(behaviour_legend.keys())

larva_tracker_dir = sys.argv[1]

os.chdir(larva_tracker_dir)

plate_directories = os.listdir()


def generate_trans_prob(datalist, nlabels):
    prob_list = [[1 for j in range(nlabels)] for i in range(nlabels)]

    for r in range(len(datalist) - 1):
        if (datalist[r] - 1) != (datalist[r + 1] - 1):
            prob_list[datalist[r] - 1][datalist[r + 1] - 1] += 1

    prob_final = [np.divide(np.array(list), sum(list)) for list in prob_list]

    return prob_final


def generate_behaviour_frequency(datalist, nlabels):
    prob_list = [1 for j in range(nlabels)]

    for r in range(len(datalist) - 1):
        prob_list[datalist[r] - 1] += 1

    prob_final = np.divide(np.array(prob_list), sum(prob_list))

    return prob_final


def compute_prob(labeldict, mode):
    n_data = len(labeldict["data"])
    label_list_list = [labeldict["data"][n]["labels"] for n in range(n_data)]
    larva_id = [labeldict["data"][n]["id"] for n in range(n_data)]

    if mode == "trans":
        transition_array_3d = np.zeros([len(label_list_list), 7, 7])
        for l in range(len(label_list_list)):
            prob_array = generate_trans_prob(label_list_list[l], 7)
            transition_array_3d[l] = prob_array
            transition_array_3d[l] = np.append(
                prob_array, [larva_id for i in prob_array.index]
            )

        return transition_array_3d

    elif mode == "bhv":
        behaviour_array_2d = np.zeros([len(label_list_list), 7])
        for l in range(len(label_list_list)):
            prob_array = generate_behaviour_frequency(label_list_list[l], 7)
            behaviour_array_2d[l] = prob_array

        return behaviour_array_2d


def process_files(filename, legend_list, mode):
    if mode == "bhv":
        with open(filename, "r") as f:
            measurement_dict = json.load(f)

        measurement_array = compute_prob(measurement_dict, mode)

        prob_df = pd.DataFrame(
            measurement_array,
            columns=legend_list,
            index=[
                measurement_dict["data"][n]["id"]
                for n in range(len(measurement_dict["data"]))
            ],
        )

        prob_df.to_csv("Behaviour_LT_" + filename + "_larva_data.csv", header=True)

        return prob_df


for plate in plate_directories:
    if os.path.isdir(plate):
        print(plate)
        os.chdir(plate)

        well_directories = os.listdir()

        for well in well_directories:
            if os.path.isdir(well):
                print(well)
                os.chdir(well)

                # Import files and filter for .label files

                label_files = os.listdir()
                label_files = [
                    file for file in label_files if file.find(".label") != -1
                ]
                if len(label_files) > 0:

                    label_file = label_files[0]
                    full_array = process_files(
                        filename=label_files[0], legend_list=label_list, mode="bhv"
                    )
                    full_array.to_csv(
                        "Behaviour_LT_" + label_files[0] + "_larva_data.csv",
                        header=True,
                    )

                os.chdir("..")
        os.chdir("..")

print("Converted LarvaTagger data to csv.")
